<?php namespace VariusApi;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class VariusApi {

    protected $http_client;

    protected $api_version = 'v1';

    protected $client_code;
    protected $acces_token;

    public function __construct($client_code, $base_uri, $access_token) {

        $this->client_code = $client_code;
        $this->access_token = $access_token;
        $this->http_client = new Client([
                'base_uri' =>  $base_uri . 'api/' . $this->api_version . '/',
                //'http_errors' => false, // let the consumer handle how to deal with non-200 responses
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $access_token
                ]
            ]);
    }

    public function status() {
        return $this->_transformResponse($this->http_client->get('status'));
    }

    public function authTest() {
        $response = $this->http_client->get($this->client_code . '/checkauth');
        return $this->_transformResponse($response);
    }

    public function getMakes() {
        $response = $this->http_client->get('makes');
        return $this->_transformResponse($response);
    }

    public function getMakesRanked($min_count=1) {
        $response = $this->http_client->get($this->client_code . "/makes/ranked");
        return $this->_transformResponse($response);
    }

    public function getMakeRanked($make_code, $min_count=1) {
        $response = $this->http_client->get($this->client_code . "/make/" . rawurlencode($make_code) . "/ranked?min_count=" . $min_count);
        return $this->_transformResponse($response);
    }

    public function getTypes() {
        $response = $this->http_client->get('types');
        return $this->_transformResponse($response);
    }

    public function getTypeRanked($type_code, $min_count=1) {
        $response = $this->http_client->get($this->client_code . "/type/" . rawurlencode($type_code) . "/ranked?min_count=" . $min_count);
        return $this->_transformResponse($response);
    }

    public function getApiTypegroupTypeRanked($type_code, $typegroup_code="", $min_count=1) {
        $response = $this->http_client->get($this->client_code . "/types/api/" . rawurlencode($typegroup_code) . "/" . rawurlencode($type_code) . "/ranked?min_count=" . $min_count);
        return $this->_transformResponse($response);
    }

    public function getApiTypegroupRanked($typegroup_code, $min_count=1) {
        $response = $this->http_client->get($this->client_code . "/types/api/" . rawurlencode($typegroup_code) . "/ranked?min_count=" . $min_count);

        return $this->_transformResponse($response);
    }

    public function getApiTypegroupsRanked($min_count=1) {

        $response = $this->http_client->get($this->client_code . '/types/api/ranked?min_count=' . $min_count);
        return $this->_transformResponse($response);
    }

    public function getListing($stock_number) {
        $response = $this->http_client->get($this->client_code . '/listing/' . rawurlencode($stock_number));
        return $this->_transformResponse($response);
    }

    public function searchListings($search_params) {

        $query_vars = [];

        $response = $this->http_client->post(
                $this->client_code . '/listings/search',
                [
                    'form_params' => $search_params
                ]
            );
        return $this->_transformResponse($response);
    }

    protected function _transformResponse($response) {
        return (object)[
            'status' => $response->getStatusCode(),
            'reasonPhrase' => $response->getReasonPhrase(),
            'body' => json_decode($response->getBody())
        ];
    }

}